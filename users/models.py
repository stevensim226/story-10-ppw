from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.TextField()
    profile_picture_url = models.URLField(blank = True, default = "https://via.placeholder.com/150/ffffff/808080%20C/")
    description = models.TextField(blank = True, default = "Hello World!, こんにちは！")

    def __str__(self):
        return f"{self.user.username}'s Profile"