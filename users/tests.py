from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve

from .views import login_view, register_view

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .models import Profile


from django.contrib.auth.models import User

class TestModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("testnewuser", password="passfortesting123")
        self.new_user.save()

        self.new_profile = Profile.objects.create(
            user = self.new_user
        )

    def test_instance_created(self):
        self.assertEquals(Profile.objects.count(), 1)

    def test_instance_is_correct(self):
        self.assertEquals(Profile.objects.first().user, self.new_user)

    def test_to_string(self):
        self.assertIn("testnewuser's Profile", str(self.new_user.profile))

class TestUrls(TestCase):
    def setUp(self):
        self.login_url = reverse("users:login_view")
        self.register_url = reverse("users:register_view")
    
    def test_login_url(self):
        login_func = resolve(self.login_url).func
        register_func = resolve(self.register_url).func
        self.assertEquals(login_func, login_view)
        self.assertEquals(register_func, register_view)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_url = reverse("users:login_view")
        self.register_url = reverse("users:register_view")
        self.logout_url = reverse("users:logout_view")

    def test_login_gives_correct_template(self):
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/login.html")

    def test_register_gives_correct_template(self):
        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/register.html")

    def test_register_login_logout_user(self):
        response = self.client.post(self.register_url, data = {
            "username" : "newuser123",
            "password1" : "samplepass999",
            "password2" : "samplepass999"
        }, follow=True)
        # check if valid user registration redirects to homepage
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "newuser123")
        self.assertTemplateUsed(response, "greeter/index.html")

        # test logout after auto login on registering
        response = self.client.post(self.logout_url, data = {}, follow = True)
        self.assertContains(response, "Anonim")
        self.assertTemplateUsed(response, "greeter/index.html")

        # test login after logging out
        response = self.client.post(self.login_url, data= {
            "username" : "newuser123",
            "password" : "samplepass999"
        }, follow = True)
        self.assertTemplateUsed(response, "users/profile.html")
        self.assertContains(response, "newuser123")

        # test whether login and registration page redirects if logged in
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 302)

        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code, 302)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_register(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/accounts/register")
        wait = WebDriverWait(selenium, 3)

        # Register new User
        username_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_username')))
        password1_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password1')))
        password2_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password2')))

        username_input.send_keys("testnewuser22")
        password1_input.send_keys("newpass999")
        password2_input.send_keys("newpass999")

        submit_registration_button = wait.until(EC.element_to_be_clickable((By.ID, "btn_register")))
        submit_registration_button.send_keys(Keys.RETURN)

        # Check if automatically logged in
        self.assertIn("testnewuser22", selenium.page_source)

    def test_login_logout(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/accounts/register")
        wait = WebDriverWait(selenium, 3)

        user = User.objects.create_user("testnewuser", password="passfortesting123")
        user.save()

        selenium.get(self.live_server_url + "/accounts/login")
        username_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_username')))
        password_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))

        username_input.send_keys("testnewuser")
        password_input.send_keys("passfortesting123")

        login_button = wait.until(EC.element_to_be_clickable((By.ID, "btn_login")))
        login_button.send_keys(Keys.RETURN)

        selenium.get(self.live_server_url)

        self.assertIn("testnewuser",selenium.page_source)

        selenium.get(self.live_server_url + "/accounts/logout")

        self.assertIn("Anonim",selenium.page_source)

    def test_invalid_register(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/accounts/register")
        wait = WebDriverWait(selenium, 3)

        # Register new User
        username_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_username')))
        password1_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password1')))
        password2_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password2')))

        username_input.send_keys("badname")
        password1_input.send_keys("badpass")
        password2_input.send_keys("badpass")

        submit_registration_button = wait.until(EC.element_to_be_clickable((By.ID, "btn_register")))
        submit_registration_button.send_keys(Keys.RETURN)

        self.assertIn("Please enter valid a name and password.", selenium.page_source)

    def test_invalid_login(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/accounts/register")
        wait = WebDriverWait(selenium, 3)

        selenium.get(self.live_server_url + "/accounts/login")
        username_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_username')))
        password_input = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))

        username_input.send_keys("badname")
        password_input.send_keys("badpass")

        login_button = wait.until(EC.element_to_be_clickable((By.ID, "btn_login")))
        login_button.send_keys(Keys.RETURN)

        self.assertIn("Invalid Username or Password.", selenium.page_source)

