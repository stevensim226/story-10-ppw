from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve

from .views import mainpage_view

class TestUrls(TestCase):
    def setUp(self):
        self.mainpage_url = reverse("greeter:mainpage")
    
    def test_mainpage_url(self):
        mainpage_func = resolve(self.mainpage_url).func
        self.assertEquals(mainpage_func, mainpage_view)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.mainpage_url = reverse("greeter:mainpage")

    def test_mainpage_gives_correct_template(self):
        response = self.client.get(self.mainpage_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "greeter/index.html")
