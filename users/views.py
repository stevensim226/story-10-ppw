from django.shortcuts import render, redirect, HttpResponse
from .forms import RegistrationForm, LoginForm, AuthenticationForm

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

from .models import Profile

def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("greeter:mainpage")

def login_view(request):
    context = {
        "login_form" : LoginForm
    }

    if request.user.is_authenticated:
        return redirect("greeter:mainpage")

    if request.method == "POST":
        login_form = LoginForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login(request, user)
            return redirect("users:profile_view",username=user.username)
        elif login_form.errors:
            context["error"] = "Invalid Username or Password."
    return render(request, "users/login.html", context)

def register_view(request):
    if request.user.is_authenticated:
        return redirect("greeter:mainpage")

    context = {
        "register_form" : RegistrationForm
    }

    if request.method == "POST":
        register_form = RegistrationForm(request.POST)
        if register_form.is_valid():
            register_form.save()
            new_user = authenticate(
                username = register_form.cleaned_data['username'],
                password = register_form.cleaned_data['password1']
            )
            new_profile = Profile.objects.create(
                user = new_user,
                full_name = new_user.username
            )
            login(request, new_user)

            return redirect("greeter:mainpage")
        elif register_form.errors:
            context["error"] = "Please enter valid a name and password."
    
    return render(request, "users/register.html", context)

def user_profile_view(request, username):
    user = User.objects.get(username = username)
    context = {
        "profile" : Profile.objects.get(user = user)
    }
    return render(request, "users/profile.html", context)