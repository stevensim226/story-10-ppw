from django.shortcuts import render, HttpResponse

# Create your views here.
def mainpage_view(request):
    context = {
        "username" : "Anonim" if not request.user.is_authenticated else request.user.username
    }
    return render(request, "greeter/index.html", context)