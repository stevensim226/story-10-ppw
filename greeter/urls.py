from django.contrib import admin
from django.urls import path

from . import views

app_name = "greeter"

urlpatterns = [
    path("", views.mainpage_view, name = "mainpage")
]